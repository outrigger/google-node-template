/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, March 2018
 */

  'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const googleapis = require('googleapis');

function getModel () {
  return require(`./model-${require('../config').get('DATA_BACKEND')}`);
}

const router = express.Router();

// Automatically parse request body as JSON
router.use(bodyParser.json());

/**
  * GET /api/contacts
  *
  * Retrieve a page of contacts .
  */
  router.get('/contacts', (req, res, next) => {
    googleapis.people('v1').people.connections.list({
      resourceName: 'people/me',
      personFields: 'emailAddresses, names',
      auth: oauth2Client },
      (err, entity) => {
        // do your thing
        if (err){
          next(err);
          return;
        }
        else {
          res.json(entity);
        }

      }
    );
  });

/**
  * POST /api/contacts
  *
  * Create a new contact.
  */
  router.post('/', (req, res, next) => {
    getModel().create(req.body, true, (err, entity) => {
      if (err) {
        next(err);
        return;
      }
      res.json(entity);
    });
  });

/**
  * GET /api/contacts/:id
  *
  * Retrieve a contact.
  */
  router.get('/:contact', (req, res, next) => {
    getModel().read(req.params.contact, (err, entity) => {
      if (err) {
        next(err);
        return;
      }
      res.json(entity);
    });
  });

/**
  * PUT /api/contacts/:id
  *
  * Update a contact.
  */
  router.put('/:contact', (req, res, next) => {
    getModel().update(req.params.contact, req.body, true, (err, entity) => {
      if (err) {
        next(err);
        return;
      }
      res.json(entity);
    });
  });

/**
  * DELETE /api/contacts/:id
  *
  * Delete a contact.
  */
  router.delete('/:contact', (req, res, next) => {
    getModel().delete(req.params.contact, (err) => {
      if (err) {
        next(err);
        return;
      }
      res.status(200).send('OK');
    });
  });

/**
  * Errors on "/api/contacts/*" routes.
  */
  router.use((err, req, res, next) => {
    // Format error and forward to generic error handler for logging and
    // responding to the request
    err.response = {
      message: err.message,
      internalCode: err.code
    };
    next(err);
  });

module.exports = router;
