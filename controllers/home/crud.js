/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, March 2018
 */

  'use strict';

const express = require('express');
const images = require('../../lib/images');
const oauth2 = require('../../lib/oauth2');


const router = express.Router();

// Use the oauth middleware to automatically get the user's profile
// information and expose login/logout URLs to templates.
  router.use(oauth2.template);
  router.use(oauth2.required);

// Set Content-Type for all responses for these routes
router.use((req, res, next) => {
  res.set('Content-Type', 'text/html');
  next();
});

/**
  * GET /contacts
  *
  * Display a page of contacts (up to ten at a time).
  */
  router.get('/', (req, res, next) => {
      res.render('home.pug', {
      });
    });

router.get('/no-invite', function (req, res, next) {
  res.render('noinvite.pug');
});


/**
  * Errors on "/contacts/*" routes.
  */
  router.use((err, req, res, next) => {
    // Format error and forward to generic error handler for logging and
    // responding to the request
    err.response = err.message;
    next(err);
  });

module.exports = router;
