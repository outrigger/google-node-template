/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, March 2018
 */

// CRUD API Settings page

'use strict';

var debug = true;
// var debug = false;

const express = require('express');
const images = require('../../lib/images');
const oauth2 = require('../../lib/oauth2');
const models = require('../../models/models');
const mongo = require('../../lib/mongo');
const notifier = require('../../lib/notify/notifier');


const router = express.Router();

var User = models.connection.collection('users');
// Use the oauth middleware to automatically get the user's profile
// information and expose login/logout URLs to templates.
router.use(oauth2.template);

// Set Content-Type for all responses for these routes
router.use((req, res, next) => {
  res.set('Content-Type', 'text/html');
  next();
});

function replaceAll (string, search, replacement) {
  var target = string;
  return target.replace(new RegExp(search, 'g'), replacement);
}

/**
 * GET /settings
 *
 * Display a page of settings (up to ten at a time).
 */
router.get('/', (req, res, next) => {
  if (!req.user){
    req.session.oauth2return = req.originalUrl;
    res.redirect('/auth/login');
  }
  else {
    if (debug){
      console.log(req.user);
    }

    //router.use(oauth2.required);
    router.use(oauth2.required);
    // Populate user data into form.
    var savuser;
    User.findOne({id: req.user.id }, function(err, user) {
      if (user){

        savuser = user;
        //console.log(savuser);
        res.locals.user = user;//res.json(user);

        // Clean up <br> tags for textarea use!
        if (res.locals.user.responseMessage){
          res.locals.user.responseMessage.message1 = replaceAll(res.locals.user.responseMessage.message1, '<br>', '\n');
          res.locals.user.responseMessage.message2 = replaceAll(res.locals.user.responseMessage.message2, '<br>', '\n');
        }


        // Add commas back in
        if (res.locals.user.config.Keyphrases){
          res.locals.user.config.Keyphrases = res.locals.user.config.Keyphrases.join(', ');
        }
        if (res.locals.user.config.whitelist){
          res.locals.user.config.whitelist = res.locals.user.config.whitelist.join(', ');
        }
        if (res.locals.user.config.blacklist){
          res.locals.user.config.blacklist = res.locals.user.config.blacklist.join(', ');
        }

        res.render('settings/form.pug', {
          user: savuser
        });

      }
      else {
        req.session.destroy();
        req.logout();
        res.redirect('/noinvite');
      }
    });
  }
});

//});

// Use the oauth2.required middleware to ensure that only logged-in users
// can access this handler.
/**
 * GET /settings/save
 *
 * Display a form for creating a contact.
 */
router.get('/save', (req, res) => {
  if (!req.user) {
    req.session.oauth2return = req.originalUrl;
    res.redirect('/auth/login');
  }
  res.render('settings/form.pug', {
    contact: {},
    action: 'save'
  });
});

/**
 * POST /settings/save
 *
 * Create a contact.
 */
// [START save]
router.post(
  '/save',
  (req, res, next) => {
    if (req.body){

    User.findOne({id: req.user.id }, function(err, user) {
      const data = req.body;
      var on, delayon, contacton, periscopeon, importon, bccon, ccon, mailon, responderon, vipon, blackon, keyon;

      var number;


      if (debug){
        console.info("Phonenumber: " + data.phonenumber);
        console.log("DB Phone: " + user.notify.sms.phoneNumber);
      }

      if (data.phonenumber == null || data.phonenumber == undefined || data.phonenumber == ""){
        if (debug){
          console.log("Updating number");
        }
        number == user.notify.sms.phoneNumber;
      }
      else {
        if (debug){
          console.log("Using from form");
        }
        number = data.phonenumber;
      }

      // Save the masterSwitch data as a boolean
      if (data.masterSwitch == 'on'){
        on = true;

        console.log('Master switch on');
        // setTimeout(require('./publisher').sendMessage(req.user.id, true), 1500);
        require('./publisher').sendMessage(req.user.id, true);
      }
      else {
        on = false;
        console.log('Master switch off');
        // setTimeout(require('./publisher').sendMessage(req.user.id, false), 1500);
        require('./publisher').sendMessage(req.user.id, false);
      }
      if (data.keySwitch == 'on'){
        keyon = true;
      }
      else {
        keyon = false;
      }
      if (data.responderSwitch == 'on'){
        responderon = true;
      }
      else {
        responderon = false;
      }
      if (data.vipSwitch == 'on'){
        vipon = true;
      }
      else {
        vipon = false;
      }
      if (data.blackSwitch == 'on'){
        blackon = true;
      }
      else {
        blackon = false;
      }
      // Save the masterSwitch data as a boolean
      if (data.periscopeSwitch == 'on'){
        periscopeon = true;
      }
      else {
        periscopeon = false;
      }
      if (data.delaySwitch == 'on'){
        delayon = true;
      }
      else {
        delayon = false;
      }
      if (data.contactSwitch == 'on'){
        contacton = true;
      }
      else {
        contacton = false;
      }
      if (data.importanceSwitch == 'on'){
        importon = true;
      }
      else {
        importon = false;
      }
      if (data.ignoreCCSwitch == 'on'){
        ccon = true;
      }
      else {
        ccon = false;
      }
      if (data.ignoreBCCSwitch == 'on'){
        bccon = true;
      }
      else {
        bccon = false;
      }
      if (data.ignoreMailingListsSwitch == 'on'){
        mailon = true;
      }
      else {
        mailon = false;
      }


      var tgact, smsact;
      if (data.notifyop == 'Telegram'){
        tgact = true;
        smsact = false;
      }
      else if (data.notifyop == 'SMS'){
        tgact = false;
        smsact = true;
      }

      // if (data.delaytime < 0.25){
      //   data.delaytime = 0.25;
      // }
      if (debug){
        console.log(data.whitelist.split(' ').join('').split(','));
      }

      var splitKeys;

      if (data.Keyphrases != undefined){
        splitKeys = data.Keyphrases.split(', ');
      }

      // If the user is logged in, set them as the creator of the contact.
      user = User.findOneAndUpdate({id: req.user.id }, {$set:{
        outriggerEnabled: on,
        notifyop: data.notifyop,
        delayEnabled: delayon,
        delaytime: data.delaytime,
        restrictContacts: contacton,
        importantOnly: importon,
        // Do not delete structure of notify
        'notify': {
          sms: {
            phoneNumber: number,
            active: smsact
          },
          slack: {

          },
          telegram: {
            msg_id: user.notify.telegram.msg_id,
            auth: user.notify.telegram.auth,
            active: tgact
          }
        },
        responseMessage: {
          prefix: data.prefix,
          message1: data.message1.replace(/(?:\r\n|\r|\n)/g, '<br>'),
          message2: data.message2.replace(/(?:\r\n|\r|\n)/g, '<br>')
        },
        'config.responder': responderon,
        'config.whitelistToggle': vipon,
        'config.blacklistToggle': blackon,
        'config.periscope': periscopeon,
        'config.periscopeTime': data.periscopeTime,
        'config.whitelist': data.whitelist.split(' ').join('').split(','),
        'config.blacklist': data.blacklist.split(' ').join('').split(','),
        'config.Keyphrases': splitKeys,
        'config.keywords': keyon,
        ignoreBCC: bccon,
        ignoreCC: ccon,
        ignoreMailingLists: mailon,
        tgact: tgact,
        smsact: smsact
      }}, function(err, user) {
        if (err){
          console.log('Error save user settings');
        }
        else{
          req.flash('Success', 'Sucess! Your settings were saved.');
          res.redirect('/settings');
        }
      });

    });
    }
    else {
      res.redirect('/');
    }
  });
// [END add]

/**
 * Errors on "/settings/*" routes.
 */
router.use((err, req, res, next) => {
  // Format error and forward to generic error handler for logging and
  // responding to the request
  err.response = err.message;
  next(err);
});

module.exports = router;
