/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, March 2018
 */

const config = require('../../config');
const crypto = require('crypto');

const models = require('../../models/models');

var User = models.connection.collection('users');

function sendMessage(userid, state){
  User.findOne({id: userid}, (err, user) => {
    // Imports the Google Cloud client library
    const PubSub = require(`@google-cloud/pubsub`);

    // Creates a client
    const pubsub = new PubSub({
      projectId: config.get('GCLOUD_PROJECT')
    });

    var token = crypto.randomBytes(6).toString("hex");
    User.findOneAndUpdate({id: userid},{$set: {'config.saveToken': token}});

    const topicName = 'enabler';
    const data = JSON.stringify({ id: userid, state: state, token: token});

    // Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
    const dataBuffer = Buffer.from(data);

    // Do not double up processes
    if ((state == true && user.isActive == false) || (state == false && user.isActive == true)){
      console.log('Publishing Message')

      pubsub
        .topic(topicName)
        .publisher()
        .publish(dataBuffer)
        .then(results => {
          const messageId = results[0];
          console.log(`Message ${messageId} published.`);
        })
        .catch(err => {
          console.error('ERROR:', err);
        });
    }
    else {
      console.error('User already enabled/Disabled.');
    }
  });

}

module.exports = {
  sendMessage: sendMessage
}
