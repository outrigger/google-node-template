/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, March 2018
 */

  'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const google = require('googleapis');


function getModel () {
  return require(`./model-${require('../config').get('DATA_BACKEND')}`);
}

const router = express.Router();

// Automatically parse request body as JSON
router.use(bodyParser.json());

/**
  * GET /api/gmail
  *
  * Retrieve a page of gmail (up to ten at a time).
  */
  router.get('https://www.google.com/m8/feeds/gmail/${userEmail}/full', (req, res, next) => {
    getModel().list(10, req.query.pageToken, (err, entities, cursor) => {
      if (err) {
        next(err);
        return;
      }
      res.json({
        items: entities,
        nextPageToken: cursor
      });
    });
  });

/**
  * POST /api/gmail
  *
  * Create a new contact.
  */
  router.post('/', (req, res, next) => {
    getModel().create(req.body, true, (err, entity) => {
      if (err) {
        next(err);
        return;
      }
      res.json(entity);
    });
  });

/**
  * GET /api/gmail/:id
  *
  * Retrieve a contact.
  */
  router.get('/:contact', (req, res, next) => {
    getModel().read(req.params.contact, (err, entity) => {
      if (err) {
        next(err);
        return;
      }
      res.json(entity);
    });
  });

/**
  * PUT /api/gmail/:id
  *
  * Update a contact.
  */
  router.put('/:contact', (req, res, next) => {
    getModel().update(req.params.contact, req.body, true, (err, entity) => {
      if (err) {
        next(err);
        return;
      }
      res.json(entity);
    });
  });

/**
  * DELETE /api/gmail/:id
  *
  * Delete a contact.
  */
  router.delete('/:contact', (req, res, next) => {
    getModel().delete(req.params.contact, (err) => {
      if (err) {
        next(err);
        return;
      }
      res.status(200).send('OK');
    });
  });

/**
  * Errors on "/api/gmail/*" routes.
  */
  router.use((err, req, res, next) => {
    // Format error and forward to generic error handler for logging and
    // responding to the request
    err.response = {
      message: err.message,
      internalCode: err.code
    };
    next(err);
  });

module.exports = router;
