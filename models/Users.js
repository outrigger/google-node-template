// Mongoose
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var collection = mongoose.collection;


mongoose.connect('mongodb://admin:outrigger@ds249428.mlab.com:49428/outrigger');
var connection = mongoose.connection;
// console.log('Connected!');

var userSchema = new Schema({
  name: {
    type: String,
    unique: false,
    required: true
  },
  age: {
    type: Number
  },
  email: {
    type: String,
    unique: false,
    required: true,
    trim: false
  },
  respondRange: {type: [Number],index: true},

  tokens: {
    accessToken: {
      type: String,
      unique: false,
      required: false
    },
    refreshToken: {
      type: String,
      unique: false,
      required: true
    }
  },

  responseMessage: {
    // Subject/headers
    prefix: String,


    // Message (body) of email
    message1: {
      type: String,
      unique: false,
      required: false,
      trim: false

    },
    message2: {
      type: String,
      unique: false,
      required: false,
      trim: false

    }
  },
  // Master switch
  outriggerEnabled: {
    type: Boolean,
    unique: false,
    required: true
  },
  // Use "busy" in calendar to enable outrigger
  useCalendar: {
    type: Boolean,
    unique: false,
    required: true
  },
  // Attempt to guess contact tiers
  aiEnabled: {
    type: Boolean,
    unique: false,
    required: true
  },
  id: {
    type: String,
    unique: true,
    required: true
  },
  notifylink: {
    type: [String],
    index: true
  },
  idBlacklist: {
    type: [String],
    index: true
  },
  delayedEmails: [{
    index: Number,
    add: String,
    tmstp: Date
  }],
  delaytime: Number,
  delayEnabled: Boolean,
  restrictContacts: Boolean,
  importantOnly: Boolean,
  topic: String,
  isActive: {
    type: Boolean,
    required: true
  },
  startId: String,
  notifyop: String,
  notify: {
    telegram: {
      msg_id: String,
      auth: String,
      active: Boolean
    },
    slack: {

    },
    sms: {
      phoneNumber: String,
      active: Boolean
    }
  },
  ignoreBCC: Boolean,
  ignoreCC: Boolean,
  ignoreMailingLists: Boolean,
  tgact: Boolean,
  smsact: Boolean,
  aliases: [{
    add: String,
    name: String,
    default: Boolean
  }],
  sessid: String,
  historyId: Number,
  admin: Boolean,
  config: {
    // TODO move more config options into here

    responder: Boolean,
    // White/black list
    whitelistToggle: Boolean,
    blacklistToggle: Boolean,
    whitelist: [String],
    blacklist: [String],

    // peiscope
    periscope: Boolean,
    periscopeTime: Number,
    periscopeList: [{
      add: String,
      tmstp: Date
    }],
    // Save token vefication
    saveToken: String,

    // Keyphrases
    keywords: Boolean,
    Keyphrases: [String]
  },
  stats: {
    sentReplies: Number,
    urgetNotifications: Number,
    periscopeNotifications: Number,
    vipNotifications: Number
  }
});



module.exports ={
  User: userSchema,
  collection: collection,
  connection: connection,
  mongoose: mongoose
};
