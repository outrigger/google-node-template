var mongoose = require("mongoose");
var Users = require("./Users"); /* Imports the Users module. It contains the User schema we need. */
var config = require("../config");

// Start [Connection]
mongoose.connect(config.get("MONGO_URL"));
// console.log('Connected!');

var db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", function(callback){
  //console.log("Connection Succeeded."); [> Once the database connection has succeeded, the code in db.once is executed. <]
});
// End [Connection]

// Create new collections in database
var User = mongoose.model("users", Users.User); //This creates the User model.

module.exports = {
  User: User, /* Export the User model so index.js can access it. */
  connection: db
};
