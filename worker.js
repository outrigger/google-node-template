/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

/**
 * Enabler for outrigger
 * @module /worker
 */

// Setup debugging
var config    = require('./config');
const debug = config.get('DEBUG');


// TODO Rewrite autoresponder to handle tiers and queueNotifications
// [START pubsub_listen_messages]
// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);
const {GoogleApis} = require('googleapis');
const google = new GoogleApis();

// Creates a client
const pubsub = new PubSub({
  projectId: config.get('GCLOUD_PROJECT'),
  keyFilename: config.get('KEYFILE'),
});


/**
 * models: used to get user/notification schemas from /models
 */
const models = require('./models/models');


var User = models.connection.collection('users');


/**
 * stops a user outrigger procces
 * @param {Object} data - takes in data containing Userid and state
 */
function kill(data){
  // Deletes the subscription
  //const subscription = pubsub.topic('gmail').subscription('Outrigger' + data.id);
  //await subscription.removeAllListeners('message');
  User.findOne({id: data.id}, function(err, user) {

    // Send confirmation notification
    require('./lib/notify/notifier').notify(user, config.get('NAME') + ' is now disabled for ' + user.email + '.');

    // Create an empty oauth2client and fill it with the tokens from the database.
    var oauth2client = new google.auth.OAuth2(config.get('OAUTH2_CLIENT_ID'), config.get('OAUTH2_CLIENT_SECRET'));
    oauth2client.credentials = {
      access_token: user.tokens.accessToken,
      refresh_token: user.tokens.refreshToken,
      expiry_date: true
    };

    // Disable outrigger
    User.findOneAndUpdate({id: data.id}, {$set: {outriggerEnabled: false, isActive: false}});
    require('./lib/client/client').outrigger(user, oauth2client, false);

  });
} //.catch(err => { console.error(err); });

/**
 * start a user outrigger procces
 * @param {Object} data - takes in data containing Userid and state
 */
function live(data){
  // Godspeed, Rebels.
  // pubsub.topic(config.get('TOPIC_NAME')).createSubscription('Outrigger' + data.id);
  User.findOne({id: data.id}, function(err, user) {
    // TODO Make this work with all notification channels
    require('./lib/notify/notifier').notify(user, config.get('NAME') + ' is now enabled for ' + user.email + '.');

    // Create an empty oauth2client and fill it with the tokens from the database.
    var oauth2client = new google.auth.OAuth2(config.get('OAUTH2_CLIENT_ID'), config.get('OAUTH2_CLIENT_SECRET'));
    oauth2client.credentials = {
      access_token: user.tokens.accessToken,
      refresh_token: user.tokens.refreshToken,
      expiry_date: true
    };

    User.findOneAndUpdate({id: data.id}, {$set: {outriggerEnabled: true, isActive: true}});
    require('./lib/client/client').outrigger(user, oauth2client, false);

  });


}

function listen(){
  const pubsub = new PubSub({
    projectId: config.get('GCLOUD_PROJECT'),
    keyFilename: './Outrigger-913bba9fd2ae.json'
  }); 
  pubsub.topic('enabler').createSubscription('worker' + process.pid).then(results => {
    // const subscription = results[0];
    if (debug){
      console.log(`Subscription created on 'Enabler'.`);
    }
    //callback(subscriptionName, 604800, user, oauth2);
    // References an existing subscription
    const subscription = pubsub.topic('enabler').subscription('worker' + process.pid);

    // Create an event handler to handle messages
    let messageCount = 0;

    if (debug){
      console.log('Listening for changes!');
    }

    const messageHandler = message => {
      // Received Message
      console.log(`Received enabler message ${message.id}:`);
      message.ack();
      //message.ack(0);



      // Extract data from message (base64)
      var b64string = message.data;
      var data = Buffer.from(b64string, 'base64'); // Ta-da
      data = JSON.parse(data);
      console.log(data);


      try {
        User.findOne({id: data.id}, (err, user) => {

          if (data.token == user.config.saveToken) {
            User.findOneAndUpdate({id: data.id},{$set: {'config.saveToken': undefined}});
            if (data.state){
              live(data);
            }
            else {
              kill(data);
            }

          }
        });
      }
      catch(e){
        console.error('ERROR', e);
      }


    };

    // Listen for new messages until timeout is hit
    subscription.on(`message`, messageHandler);
  }).catch(err => { console.error(err); });
};
// [END pubsub_listen_messages]



function serverlessListen (message, callback) {
  console.log(`Received enabler message ${message.id}:`);
  message.ack();
  //message.ack(0);



  // Extract data from message (base64)
  var b64string = message.data;
  var data = Buffer.from(b64string, 'base64'); // Ta-da
  data = JSON.parse(data);
  console.log(data);


  try {
    if (data.state){
      live(data);
    }
    else {
      kill(data);
    }
    callback();
  }
  catch(e){
    console.error('ERROR', e);
    callback();
  }

};


module.exports = {
  listen: listen,
  onMessage: serverlessListen
}
