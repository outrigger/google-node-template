/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

  'use strict';

// Hierarchical node.js configuration with command-line arguments, environment
// variables, and files.
const nconf = module.exports = require('nconf');
const path = require('path');

nconf
// 1. Command-line arguments
  .argv()
// 2. Environment variables
  .env([
    'BETA',
    'DEBUG',
    'CLOUD_BUCKET',
    'DATA_BACKEND',
    'GCLOUD_PROJECT',
    'MONGO_URL',
    'MONGO_COLLECTION',
    'NODE_ENV',
    'OAUTH2_CLIENT_ID',
    'OAUTH2_CLIENT_SECRET',
    'OAUTH2_CALLBACK',
    'PORT',
    'SECRET',
    'SUBSCRIPTION_NAME',
    'TOPIC_NAME',
    'ENABLED',
    'TELEGRAM_API',
    'TELEGRAM_BOT_NAME',
    'SLACK_API_WEBHOOK'
  ])
// 3. Config file
  .file({ file: path.join(__dirname, 'config.json') })
// 4. Defaults
  .defaults({
    // Typically you will create a bucket with the same name as your project ID.
    CLOUD_BUCKET: '',


    // This is the id of your project in the Google Cloud Developers Console.
    GCLOUD_PROJECT: '',


    // MongoDB connection string
    // https://docs.mongodb.org/manual/reference/connection-string/
    MONGO_URL: '',
    MONGO_COLLECTION: 'books',

    OAUTH2_CLIENT_ID: '',
    OAUTH2_CLIENT_SECRET: '',
    OAUTH2_CALLBACK: 'http://localhost:8080/auth/google/callback',

    PORT: 8080,

    DEBUG: false,

    // Set this a secret string of your choosing
    SECRET: 'keyboardcat',

    SUBSCRIPTION_NAME: 'shared-worker-subscription',
    TOPIC_NAME: 'book-process-queue',

    ENABLED: true
  });

// Check for required settings
checkConfig('GCLOUD_PROJECT');
checkConfig('CLOUD_BUCKET');
checkConfig('OAUTH2_CLIENT_ID');
checkConfig('OAUTH2_CLIENT_SECRET');

  // if (nconf.get('NODE_ENV') === 'production') {
  //   checkConfig('INSTANCE_CONNECTION_NAME');
  // }

function checkConfig (setting) {
  if (!nconf.get(setting)) {
    throw new Error(`You must set ${setting} as an environment variable or in config.json!`);
  }
}
