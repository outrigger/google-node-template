/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

/**
 * MongoDB Handler
 * @module lib/mongo
 */

// Import mongodb modules
const mongoose = require('mongoose');
const {GoogleApis} = require('googleapis');
const PubSub = require(`@google-cloud/pubsub`);
const config = require('../config');
const google = new GoogleApis();
const async = require('async');
const crypto = require('crypto');
/**
 * models: used to get user/notification schemas from /models
 */
const models = require('../models/models');

/**
 * message queue
 */
const mongoDbQueue = require('mongodb-queue')

/**
 * con is the connection
 */
var con = models.connection

var User = models.connection.collection('users');


function start(){
  User.find({}, function(err, users) {

    // Creates a client
    const pubsub = new PubSub({
      projectId: config.get('GCLOUD_PROJECT'),
      keyFilename: config.get('KEYFILE') 
    });


    users.forEach(function(user) {

      // Setup an oauth2client
      var oauth2client = new google.auth.OAuth2(config.get('OAUTH2_CLIENT_ID'), config.get('OAUTH2_CLIENT_SECRET'));
      oauth2client.credentials = {
        access_token: user.tokens.accessToken,
        refresh_token: user.tokens.refreshToken,
        expiry_date: true
      };

      // TODO

      // START APP HERE
    }, (res, err) => {

      // Finished
      console.log('Startup success');
    });

  });

}

function generateOneTimeToken(user, index){
      var token;
      crypto.randomBytes(20, function(err, buf) {
        token = buf.toString('hex');
      });
      User.findOneAndUpdate({id: user.id}, {$push:{notifylink: token}}, (err, user) => {
        if (err){
          console.log(err);
        }
      });
      console.log(token);
      return token;
      }



//function setId(id){
//console.log('mongo.js got: ' + id);
//this.id = id;
//console.log('this.id: ' + this.id);
//};

module.exports = {
  generateOneTimeToken: generateOneTimeToken,
  start: start,
};
