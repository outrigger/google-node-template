/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

'use strict';

/**
 * Oauth2 for google
 * @module lib/oauth2/google
 */
// Setup debugging
var config    = require('../config');
const debug = config.get('DEBUG');

const express = require('express');
const favicon = require('static-favicon');
// Setup saving tokens to mongodb
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
//const GoogleStrategy = require('passport-google').Strategy;
const {GoogleApis} = require('googleapis');
const google = new GoogleApis();
const crypto = require("crypto");

const mongoose = require('../models/models');
const mongo = require('./mongo');


var db = mongoose.connection.db;
var newUser;
// [START setup]

// Prevent authClient.request error

/**
 * Exracts a users profile image/name/id
 * @param {var} profile - the profile object returned from the GoogleStrategy
 * @returns {var} profile - returns id, name, and url to the image
 */
function extractProfile (profile) {
  let imageUrl = '';
  if (profile.photos && profile.photos.length) {
    imageUrl = profile.photos[0].value;
  }
  return {
    id: profile.id,
    name: profile.displayName,
    image: imageUrl,

  };
}


passport.serializeUser((user, cb) => {
  cb(null, user);
});
passport.deserializeUser((obj, cb) => {
  cb(null, obj);
});


// [END setup]

const router = express.Router();


// [START middleware]
/** Middleware that requires the user to be logged in. If the user is not logged in, it will redirect the user to authorize the application and then return them to the original URL they requested. */

function authRequired (req, res, next) {
  if (!req.user) {
    req.session.oauth2return = req.originalUrl;
    return res.redirect('/auth/login');
  }
  next();
}

// TODO Write a token checker
//function checkToken(req, res, next){
////if(auths[req.session.id].expiries){}
//}


/** Middleware that exposes the user's profile as well as login/logout URLs to any templates. These are available as `profile`, `login`, and `logout`. */
function addTemplateVariables (req, res, next) {
  res.locals.profile = req.user;
  res.locals.login = `/auth/login?return=${encodeURIComponent(req.originalUrl)}`;
  res.locals.logout = `/auth/logout`;
  next();
}
// [END middleware]

// Begins the authorization flow. The user will be redirected to Google where
// they can authorize the application to have access to their basic profile
// information. Upon approval the user is redirected to `/auth/google/callback`.
// If the `return` query parameter is specified when sending a user to this URL
// then they will be redirected to that URL when the flow is finished.
// [START authorize]
router.get(
  // Login url
  '/auth/login',

  // Configure the Google strategy for use by Passport.js.
  //
  // OAuth 2-based strategies require a `verify` function which receives the
  // credential (`accessToken`) for accessing the Google API on the user's behalf,
  // along with the user's profile. The function must invoke `cb` with a user
  // object, which will be set at `req.user` in route handlers after
  // authentication.


  (req, res, next) => {
    var oauth2;
    var strategy = new GoogleStrategy({
      clientID: config.get('OAUTH2_CLIENT_ID'),
      clientSecret: config.get('OAUTH2_CLIENT_SECRET'),
      callbackURL: config.get('OAUTH2_CALLBACK')
    }, function(accessToken, refreshToken, profile, cb) {

      if (debug){
        console.log('refreshToken is ' + refreshToken);
      }

      // Extract the minimal profile information we need from the profile object
      // provided by Google
      cb(null, extractProfile(profile));

      //use schema.create to insert data into the db
      var authedUser = new mongoose.User({
        _id: req.session._id,
        tokens: {
          accessToken: accessToken,
          refreshToken: refreshToken
        },
        responseMessage: {
          prefix: "Auto-Reply:",
          message1: "Thank you for the message.\n\nI am currently away from email.\n\nIf your message is urgent, please click below to notify me:",
          message2: "Thank you, \n\n" + profile.displayName
        },
        id: profile.id,
        name: profile.displayName,
        email: profile.emails[0].value,
        outriggerEnabled: config.get('ENABLED'),
        topic: ('projects/outrigger-170622/topics/gmail' + profile.id),
        config: {
          periscope: true,
          periscopeTime: 15,
          periscopeList: [],
          whitelist: [],
          blacklist: []
        },
        idBlacklist: [],
        restrictContacts: false,
        delayedEmails: [],
        delaytime: 4,
        delayEnabled: true,
        importantOnly: false,
        isActive: false,
        notify: {
          telegram: {
            auth: crypto.randomBytes(6).toString('hex'),
            active: false
          },
          sms: {
            active: false,
            phonenumber: undefined
          }

        },
        //Leave disabled until implemented
        aiEnabled: false,
        useCalendar: false,
        ignoreBCC: true,
        ignoreCC: true,
        ignoreMailingLists: true,
        tgact: true,
        smsact: false
      }, function (err) {
        if (err) {
          return next(err);
        } else {
          return res.redirect('/');
        }
      });
      var checkUser = false;
      if (debug){
        console.log('Checking against id: ' + profile.id);
      }
      mongoose.User.find({id: profile.id}, function(err, users) {
        if (users.length > 0){
          newUser = false;
        }
        else {
          newUser = true;
        }

        // if (newUser ){
          if (newUser){
            if (debug){
              console.log('New user');
            }
            const PubSub = require(`@google-cloud/pubsub`);
            // Creates a client
            const pubsub = new PubSub({
              projectId: config.get('GCLOUD_PROJECT')
            });
            authedUser.save(function(err, result){
              pubsub.topic('gmail').createSubscription('Outrigger' + profile.id);
              if(err){
                if (debug){
                  console.log('>>>>>> Error');
                  console.log('Error saving user to database');
                  console.log(err);
                }
              }
              else
              {
                // console.log('>>>>>> ' + JSON.stringify(result, null, 4));
                mongoose.User.findOne({id: profile.id}, (err, user) => {
                  res.locals.user = user;
                  console.log(res.locals.user.name);
                });
              }
              // Redirect new users since we are still in beta
            });
          }
          else
          {
            if (debug){
              console.log('Not new user');
            }
            if (refreshToken){
              mongoose.User.findOneAndUpdate({id: profile.id}, {$set:{"tokens.accessToken": accessToken, "tokens.refreshToken": refreshToken}}, function (err, res){
                if (err && debug){
                  console.error('Error updating tokens.');
                }
              });
            }
            else {
              mongoose.User.findOneAndUpdate({id: profile.id}, {$set:{"tokens.accessToken": accessToken}}, function (err, res){
                if (err && debug){
                  console.error('Error updating tokens.');
                }
              });
              mongoose.User.findOne({id: profile.id}, (err, user) => {
                if (user){
                  res.locals.user = user;
                }
              });
            }
          }
      });
    });

    // Proceed with auth
    passport.use(strategy);

    // Save user in res

    // Save the url of the user's current page so the app can redirect back to
    // it after authorization
    if (req.query.return) {
      req.session.oauth2return = req.query.return;
    }
    next();
  },

  // Start OAuth 2 flow using Passport.js
  //if (config.get('BETA')){
  //if (!newUser){
  passport.authenticate('google', { scope: [
    'email',
    'profile',
    'https://www.googleapis.com/auth/calendar',
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/contacts',
    'https://www.google.com/m8/feeds'
  ],
    session: false,
    failureRedirect: "/auth/login",
    accessType: 'offline',
    prompt: 'consent'
  }, function (err, user, info){
    if (err){
      console.log("Error: " + err);
    }
    //}}
    //else {
    //res.redirect('/home/no-invite');
    //}

  }));
// [END authorize]

// [START callback]
router.get(
  // OAuth 2 callback url. Use this url to configure your OAuth client in the
  // Google Developers console
  '/auth/google/callback',

  // Generate access tokens

  // Finish OAuth 2 flow using Passport.js
  passport.authenticate('google'),
  // Redirect back to the original page, if any
  (req, res) => {
    //const redirect = req.session.oauth2return || '/';
    //delete req.session.oauth2return;

    // Save session id for serverless
    // mongoose.User.findOneAndUpdate({id: req.user.id}, {$set:{sessid: req.session.id}});

    var redirect;

    // FIXME REMOVE ME AFTER BETA!!!!
    if (beta.check(req.user.email)){
      redirect = '/settings';
      // res.send({user: req.user});
    }
    else {
      redirect = '/settings';
      // redirect = '/noinvite';
    }
    res.redirect(redirect);

  }
);
// [END callback]

// Deletes the user's credentials and profile from the session.
// This does not revoke any active tokens.
router.get('/auth/logout', (req, res) => {
  req.session.destroy();
  req.logout();
  res.redirect('/auth/red/');
});

router.get('/auth/red', (req, res) => {
  // redirect to page-b.html with 301 (Moved Permanently) HTTP code in the response
  res.writeHead(302, { "Location": 'http://outriggerapp.com' });
  return res.end();
});

module.exports = {
  router: router,
  required: authRequired,
  template: addTemplateVariables
};
