/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

/**
 *Voice call notifier
 *@module notify/call
 */

// Twilio Credentials
const accountSid = 'AC1f1ee9bbb65d278bb3c09acc6f64c09f';
const authToken = '09d047332ef19fe9a67fd328040cfd2b';
const Twilio = require('twilio');
const client = new Twilio(accountSid, authToken);

/**
 * call the user and delivers a message
 * @param {object} user - the user object
 * @param {string} message - the message to deliver.
 */
function call (user, message){
  client.api.calls
    .create({
      url: 'http://demo.twilio.com/docs/voice.xml',
      to: user.notify.sms.phoneNumber,
      from: '+12062034383',
    })
    .then(call => console.log(call.sid));
}
module.exports = {
  call: call
}
