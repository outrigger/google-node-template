/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

/** 
  * @module lib/notifier
  */

  // Pull in Outrigger notification modules
const slack = require('./notify-slack.js');
const gram = require('./notify-telegram.js');
const sms = require('./notify-sms.js');

// Snag our message queue
const queues = require('../mongo');

const mongoDbQueue = require('mongodb-queue')

const models = require('../../models/models');
var con = models.connection

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function cycleNotifications(user){
  // Punch it Chewie!
  var q = mongoDbQueue(con.db, (user.id + 'Queue'));

  // Cycle through messages and send them
  for(i = 0; i > q.total; i++){
    console.log('New message');
    var message = queues.getMessage(user.id);
    //if (user.notifyop == 'slack'){
      //slack.notify(message.payload, user);
    //}
    //else if (user.notifyop == 'telegram'){
    if (user.notify.telegram.active){
      gram.notify(user, message.payload);
    }
    //else if (user.notify == 'sms'){
      //sms.notify(message);
      //}
    sleep(1000);

  }
}


/**
 * Starts the notifiaction progress
 * @param {object} user - the user object
 * @param {string} message - the message to send
 */
function notify(user, message){
  var msg = message.split('<').join('').split('>').join('').split('\'').join('\'');
  if (user.notifyop == 'Telegram'){
    gram.notify(user, msg);
  }
  else if (user.notifyop == 'SMS'){
    sms.notify(user, msg.split('*').join(''));
  }
}


module.exports = {
  sleep: sleep,
  notify: notify,
  cycleNotifications: cycleNotifications
};
