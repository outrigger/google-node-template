/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Gabe Guatalupe, April 2018
 */

/**
 * SMS nofitier (free)
 * @module nodemailer
 */

var subject = 'Testing with a subject variable'
var message = 'Testing with a message variable'
var footer = '\n\nThis message was sent by a bot, please do not reply'
var userNumber = 'user.notify.sms.phonenumber'

var email = 'outriggerdotapp@gmail.com'
var password = '&yd*c6GU*Of8*KZg0QYszw?05qOG^dAuppCuHiYTvB|Yg^_E8nQV0mqkfKe1hA@aO4&&fO4IzXfT1QGqzdSy=+$gfvLcvL%q8Mtz'

var nodemailer = require('nodemailer') 

var addresses = ['@message.alltel.com', '@txt.att.net', '@myboostmobile.com', '@messaging.sprintpcs.com', '@tmomail.net', '@email.uscc.net', '@vtext.com', '@vmobl.com', '@text.republicwireless.com']

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: email,
    pass: password
  }
});


  for (i = 0; i < addresses.length; i++) {
    var mailOptions = {
      from: email,
      to: userNumber + addresses[i],
      subject: subject,
      text: message + footer
    };
    
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: to ' + userNumber + ' info: ' + info.response);
      };
    });
  }
