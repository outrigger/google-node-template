/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

const TelegramBot = require('node-telegram-bot-api');
//const config = require('../../config');

const models = require('../../models/models');
var User = models.connection.collection('users');



// replace the value below with the Telegram token you receive from @BotFather
//const token = config.get('TELEGRAM_API');
const token = "498517843:AAFarkaOQ_fPveS-6XiRxoWZ5MdKZZwSnYw";


// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});


//bot.on("text", (message) => {
//console.log(message.text);
//console.log('Message received')
//bot.sendMessage(message.chat.id, "Hello world");
//});

bot.on("text", (message) => {
  console.log(message.text);
  if (message.text.toLowerCase().includes('/start')){
    bot.sendMessage(message.chat.id, "Processing user...");
    console.log('Message recieved');
    auth = message.text.split(' ')[1];
    console.log(auth);


    User.findOneAndUpdate({id: auth}, {$set:{ 'notify.telegram':{ msg_id: message.chat.id, active: true }}},
      function(err, user) {
      if (err){
        console.log(err);
        bot.sendMessage(message.chat.id, 'Error Processing Telegram code. Please double check the code and try again.');
      }
      else {
        console.log('user ' + user);
        bot.sendMessage(message.chat.id, 'Success! You are now authorized to receive notifications through Telegram.');
      }
    });
  }
  else{
    bot.sendMessage(message.chat.id, "You will now receive Outrigger notifications.");
  }
});

function notify(user, message){
  var users = User.findOne({id: user.id}, (err, doc) => {
    bot.sendMessage(doc.notify.telegram.msg_id, message, {parse_mode: 'markdown'});
  });
}

module.exports = {
  notify: notify
}
//};
////}
