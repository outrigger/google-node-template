/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

/**
  * @module notify/slack
  */
  const Slack = require('slack-node');
const config = require('../../config');

/**
  * Notifies a user on slack
  * @param {string} message - the message value from the message queue
  * @param {var} user - user object from mongodb
  */
  function notify(message, user){
    const slack = new Slack();
    //slack.setWebhook(config.get('SLACK_API_WEBHOOK'));
    slack.setWebhook("https://hooks.slack.com/services/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");

    slack.webhook({
      channel: '@avery',
      username: "Outrigger",
      icon_emoji: ":warning:",
      text: message
    }, function(err, response) {
      if(err) {
        console.log(err);
      }
      else{
        console.log(response);
      }
    }); 
  }

module.exports = {
  notify: notify
}
