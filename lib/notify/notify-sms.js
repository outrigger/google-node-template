/* Copyright (C) Outrigger - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Avery Wagar <avery@wagar.cc>, May 2018
 */

/**
 *SMS notifier
 *@module notify/sms
 */


// Twilio Credentials
const accountSid = 'AC1f1ee9bbb65d278bb3c09acc6f64c09f';
const authToken = '09d047332ef19fe9a67fd328040cfd2b';

// require the Twilio module and create a REST client
const client = require('twilio')(accountSid, authToken);


/**
 * Sends an SMS notification
 * @param {object} user - the user object
 * @param {string} message - the message
 */
function notify (user, message){
  if (user.notify.sms.phoneNumber){
    client.messages.create({
      to: user.notify.sms.phoneNumber,
      from: '+12062034383',
      body: message
    }).then(message => console.log(message.sid)).catch(error => console.error(error));
  }
}
module.exports = {
  notify: notify
}
