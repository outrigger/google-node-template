// index.js
'use strict';

/* ----------- START Stack -------------- */

//const serverless = require('serverless-http');


const config = require('./config');
const debug = config.get('DEBUG');
const models = require('./models/models');
const mongo  = require('./lib/mongo');
/**
 * Use custom logging
 */
const logging  = require('./lib/logging');


// const spdy  = require('spdy');

// const cors  = require('cors');

// const http2 = require('http2');


// Express stack
const fs           = require('fs');
const compression  = require('compression');
const express      = require('express');
const favicon      = require('serve-favicon');
const path         = require('path');
const cookieparser = require('cookie-parser');
const bodyParser   = require('body-parser');
//Sessions!
const session      = require('express-session');
const MongoStore   = require('connect-mongo')(session);
const uid          = require('uid-safe');
const async        = require('async');

const flash        = require('express-flash');
const passport     = require('passport');

const app          = express();

/**
 * Setup compression
 */
app.use(compression());

// Serve a custom favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

var User = models.connection.collection('users');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieparser());

// Configure the session and session storage.
var db = models.connection;
/**
 * Setup a sessionConfig for later use
 */
var sessionConfig = session({
  genid: function(req) {
    return uid.sync(18); // use UUIDs for session IDs
  },
  resave: true,
  saveUninitialized: true,
  secret: config.get('SECRET'),
  signed: true,
  unset: 'destroy',
  cookie: {
    secure: true,
    maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
  },
  store: new MongoStore({ mongooseConnection: db })
});

// Start [Sessions]
app.use(sessionConfig);
app.use(flash());

/*----------------------- END Stack ---------------------------*/

/*------------------------ Start Routing --------------------- */ 
// Save

// Activate Google Cloud Trace and Debug when in production
if (process.env.NODE_ENV === 'production') {
  // Add the request logger before anything else so that it can
  // accurately log requests.
  app.use(logging.requestLogger);

  // Add the error logger after all middleware and routes so that
  // it can log errors from the whole application. Any custom error
  // handlers should go after this.
  app.use(logging.errorLogger);
  app.set('trust proxy', 1); // trust first proxy
  var http  = require('http');
  var https = require('https');

  http.globalAgent.maxSockets  = Infinity;
  https.globalAgent.maxSockets = Infinity;

  if (debug){
    console.log('Node.js is in PRODUCTION');
  }
}
else{
  // Add the request logger before anything else so that it can
  // accurately log requests.
  app.use(logging.requestLogger);

  // Add the error logger after all middleware and routes so that
  // it can log errors from the whole application. Any custom error
  // handlers should go after this.
  app.use(logging.errorLogger);

} 
// End [Sessions]

// In production use the App Engine Memcache instance to store session data,
// otherwise fallback to the default MemoryStore in development.
//if (config.get('NODE_ENV') === 'production' && config.get('MEMCACHE_URL')) {
//sessionConfig.store = new MemcachedStore({
//hosts: [config.get('MEMCACHE_URL')]
//});
//}

app.disable('etag');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('trust proxy', true);

// OAuth2 (google login)
app.use(passport.initialize());
app.use(passport.session());
app.use(require('./lib/oauth2').router);
app.use(require('./lib/oauth2').template); // Middleware for login links


// Public dir for css js etc.
app.use(express.static(path.join(__dirname, '/public')));


// ROUTING

// Root 
app.get('/', require('./controllers/home/crud'));

//settings
app.use('/settings', require('./controllers/settings/crud'));

app.get('/health-check', (req, res) => res.sendStatus(200));

// Basic 404 handler
 app.use((req, res) => {
   res.status(404).redirect('/');
 });

/* ---------------------- END Routing --------------------*/

/* ---------------------- START Server -------------------*/

//Start the server
const server = app.listen(config.get('PORT'), () => {
  const port = server.address().port;
  if (debug){
    console.log(`App listening on port ${port}`);
  }

  console.info('Outrigger server started.');
});

/* -------------------- END Server ---------------------*/

module.exports = {
  server,
  app
}
